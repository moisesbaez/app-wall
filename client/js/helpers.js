"use strict";

Template.posts.helpers({
    posts: function () {
        return Collections.Posts.find({}, {sort: {date: -1}});
    },

    image: function () {
        var imageFile = Collections.Images.find({
            _id: this.imageId
        }).fetch();
        return imageFile[0];
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                percentPosition: true,
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });
    }
});