"use strict";

Template.upload.events({
    'submit .add-post': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[2].firstElementChild.lastElementChild.files[0],
            author = event.currentTarget.children[0].value,
            message = event.currentTarget.children[1].value,
            index;

        if (author === "" || message === "" || imageFile === undefined) {
            Materialize.toast("Author, Message, or Image File Missing", 3000);
            return false;
        }
        
        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                Materialize.toast("Unable to upload image, try again later", 3000);
            } else {
                Collections.Posts.insert({
                    author: author,
                    message: message,
                    imageId: fileObject._id,
                    date: new Date()
                });
                $('.grid').masonry('reloadItems');
            }
        });
    }
});